import { Suspense, Fragment, lazy } from "react";
import { Routes, Route } from "react-router-dom";

import MainLayout from "./layouts/MainLayout";

export const renderRoutes = (routes = []) => (
  <Suspense fallback={""}>
    <Routes>
      {routes.map((route, i) => {
        const Layout = route.layout || Fragment;
        const Component = route.component;

        return (
          <Route
            key={i}
            path={route.path}
            element={
              <Layout>
                <Component />
              </Layout>
            }
          />
        );
      })}
    </Routes>
  </Suspense>
);

const routes = [
  {
    path: "/dashboard",
    layout: MainLayout,
    component: lazy(() => import("./views/Dashboard")),
  },
  {
    path: "/myspace",
    layout: MainLayout,
    component: lazy(() => import("./views/MySpace")),
  },
  {
    path: "/employees",
    layout: MainLayout,
    component: lazy(() => import("./views/Employees")),
  },

  {
    path: "/leaves",
    layout: MainLayout,
    component: lazy(() => import("./views/Leaves")),
  },
  {
    path: "/settings",
    layout: MainLayout,
    component: lazy(() => import("./views/Settings")),
  },
  {
    path: "*",
    layout: MainLayout,
    component: lazy(() => import("./views/NotFound")),
  },
];

export default routes;
