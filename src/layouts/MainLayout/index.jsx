import Sidebar from "./Sidebar";
import Header from "./Header";
import "./_index.scss";
const MainLayout = ({ children }) => {
  return (
    <div className="main-layout">
      <Sidebar />
      <div className="container">
        <Header />
        {children}
      </div>
    </div>
  );
};

export default MainLayout;
